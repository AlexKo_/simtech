{include file="common/subheader.tpl" title=__("Condition") target="#condition_product_setting"}
<div id="condition_product_setting" class="in collapse">
	<fieldset>
		<label for="condit_obj_type" class="control-label">{"Состояние"}:</label>
		<div class="controls">
		
			{$conditions_types_list = array('destroyed', 'poor', 'good', 'average', 'excellent')}
			{$conditions_type = $product_data.prod_condit}
			<select name="product_data[prod_condit]" id="prod_condit">
				{foreach $conditions_types_list as $type => $type_name}
					<option {if $conditions_type == $type_name}selected="selected"{/if} value="{$type_name}">{$type_name}</option>
				{/foreach}
			</select>
		</div>
	</fieldset>
</div>


